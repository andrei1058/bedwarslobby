package com.andrei1058.bedwarslobby.database;

import com.andrei1058.bedwarslobby.SpigotMain;
import com.andrei1058.bedwarslobby.language.Language;
import com.andrei1058.bedwarslobby.stats.StatsCache;
import com.andrei1058.bedwarslobby.stats.StatsManager;

import java.sql.*;
import java.util.UUID;

public class MySQL implements Database {

    private Connection connection;
    private String host, database, user, pass;
    private int port;
    private boolean ssl;

    /**
     * Create new MySQL connection.
     */
    public MySQL() {
        this.host = SpigotMain.config.getYml().getString("database.host");
        this.database = SpigotMain.config.getYml().getString("database.database");
        this.user = SpigotMain.config.getYml().getString("database.user");
        this.pass = SpigotMain.config.getYml().getString("database.pass");
        this.port = SpigotMain.config.getYml().getInt("database.port");
        this.ssl = SpigotMain.config.getYml().getBoolean("database.ssl");
    }

    /**
     * Connect to remote database.
     *
     * @return true if connected successfully.
     */
    public boolean connect() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database + "?autoReconnect=true&user=" + user
                    + "&password=" + pass + "&useSSL=" + ssl + "&useUnicode=true&characterEncoding=UTF-8");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Check if database is connected.
     */
    public boolean isConnected() {
        if (connection == null) return false;
        try {
            return !connection.isClosed();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Check if player has remote stats.
     */
    public boolean hasStats(UUID uuid) {
        if (!isConnected()) connect();
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT id FROM global_stats WHERE uuid = '" + uuid.toString() + "';");
            return rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void init() {
        if (!isConnected()) connect();
        try {
            connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS global_stats (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, " +
                    "name VARCHAR(200), uuid VARCHAR(200), first_play TIMESTAMP NULL DEFAULT NULL, " +
                    "last_play TIMESTAMP NULL DEFAULT NULL, wins INT(200), kills INT(200), " +
                    "final_kills INT(200), looses INT(200), deaths INT(200), final_deaths INT(200), beds_destroyed INT(200), games_played INT(200));");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS  player_levels (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, uuid VARCHAR(200), " +
                    "level INT(200), xp INT(200), name VARCHAR(200) CHARACTER SET utf8, next_cost INT(200));");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS player_language (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, uuid VARCHAR(200), " +
                    "iso VARCHAR(200));");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateLocalCache(UUID uuid) {
        if (!isConnected()) connect();
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT * FROM global_stats WHERE uuid = '" + uuid.toString() + "';");
            if (rs.next()) {
                StatsCache cs = StatsManager.getStatsCache();
                cs.setFirstPlay(uuid, rs.getTimestamp("first_play"));
                cs.setLastPlay(uuid, rs.getTimestamp("last_play"));
                cs.setWins(uuid, rs.getInt("wins"));
                cs.setKills(uuid, rs.getInt("kills"));
                cs.setFinalKills(uuid, rs.getInt("final_kills"));
                cs.setLosses(uuid, rs.getInt("looses"));
                cs.setDeaths(uuid, rs.getInt("deaths"));
                cs.setFinalKills(uuid, rs.getInt("final_deaths"));
                cs.setBedsDestroyed(uuid, rs.getInt("beds_destroyed"));
                cs.setGamesPlayed(uuid, rs.getInt("games_played"));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() {
        if (isConnected()) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Object[] getLevelData(UUID player) {
        if (!isConnected()) connect();
        Object[] r = new Object[]{1, 0, "", 0};
        try {
            ResultSet rs = connection.prepareStatement("SELECT level, xp, name, next_cost FROM player_levels WHERE uuid = '" + player.toString() + "';").executeQuery();
            if (rs.next()) {
                r[0] = rs.getInt("level");
                r[1] = rs.getInt("xp");
                r[2] = rs.getString("name");
                r[3] = rs.getInt("next_cost");
                if ((int)r[3] == 0) r[3] = 1000;
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return r;
    }

    @Override
    public void setLevelData(UUID player, int level, int xp) {
        if (!isConnected()) connect();
        try {
            ResultSet rs = connection.prepareStatement("SELECT id from player_levels WHERE uuid = '" + player.toString() + "';").executeQuery();
            if (!rs.next()) {
                PreparedStatement ps = connection.prepareStatement("INSERT INTO player_levels VALUES (?, ?, ?, ?, ?, ?);");
                ps.setInt(1, 0);
                ps.setString(2, player.toString());
                ps.setInt(3, level);
                ps.setInt(4, xp);
                ps.setString(5, "null");
                ps.setInt(6, 1000);
                ps.executeUpdate();
            } else {
                PreparedStatement ps = connection.prepareStatement("UPDATE player_levels SET level=?, xp=? WHERE uuid = '" + player.toString() + "';");

                ps.setInt(1, level);
                ps.setInt(2, xp);
                ps.executeUpdate();
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setLanguage(UUID player, String iso) {
        if (!isConnected()) connect();
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT iso FROM player_language WHERE uuid = '" + player.toString() + "';");
            if (rs.next()) {
                connection.createStatement().executeUpdate("UPDATE player_language SET iso='" + iso + "' WHERE uuid = '" + player.toString() + "';");
            } else {
                connection.createStatement().executeUpdate("INSERT INTO player_language VALUES (0, '" + player.toString() + "', '" + iso + "');");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getLanguage(UUID player) {
        if (!isConnected()) connect();
        String iso = Language.defaultLanguage.getIso();
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT iso FROM player_language WHERE uuid = '" + player.toString() + "';");
            if (rs.next()) {
                iso = rs.getString("iso");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return iso;
    }
}

package com.andrei1058.bedwarslobby.commads.main;

import com.andrei1058.bedwars.arena.Arena;
import com.andrei1058.bedwarslobby.arena.ArenaData;
import com.andrei1058.bedwarslobby.arena.ArenaStatus;
import com.andrei1058.bedwarslobby.commads.SubCommand;
import com.andrei1058.bedwarslobby.language.Messages;
import com.andrei1058.bedwarslobby.misc.Utils;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

import static com.andrei1058.bedwarslobby.language.Language.getMsg;

public class JoinCMD extends SubCommand {

    public JoinCMD(String name, String permission) {
        super(name, permission);
    }

    @Override
    public void execute(CommandSender s, String[] args) {
        if (s instanceof ConsoleCommandSender) return;
        Player p = (Player) s;
        if (args.length < 1) {
            s.sendMessage(getMsg(p, Messages.COMMAND_JOIN_USAGE));
            return;
        }
        if (args[0].equalsIgnoreCase("random")) {
            if (!Utils.joinRandomArena(p)) {
                s.sendMessage(getMsg(p, Messages.COMMAND_JOIN_NO_EMPTY_FOUND));
            }
            return;
        }
        if (ArenaData.hasGroup(args[0])) {
            if (!Utils.joinRandomFromGroup(p, args[0])) {
                s.sendMessage(getMsg(p, Messages.COMMAND_JOIN_NO_EMPTY_FOUND));
            }
            return;
        } else if (ArenaData.getArenaByName(args[0]) != null) {
            ArenaData a = ArenaData.getArenaByName(args[0]);
            if (a.getStatus() != ArenaStatus.RESTARTING) {
                Utils.sendPlayerToServer(p, a, false);
            }
            return;
        }
        s.sendMessage(getMsg(p, Messages.COMMAND_JOIN_GROUP_OR_ARENA_NOT_FOUND).replace("{name}", args[0]));
        return;
    }

    @Override
    public List<String> tabComplete(CommandSender s, String alias, String[] args, Location location) {
        List<String> tab = new ArrayList<>();
        for (ArenaData ad : ArenaData.getArenaByIdentifier().values()) {
            if (!tab.contains(ad.getArenaGroup())) tab.add(ad.getArenaGroup());
        }
        for (Arena arena : Arena.getArenas()) {
            tab.add(arena.getWorldName());
        }
        return tab;
    }
}

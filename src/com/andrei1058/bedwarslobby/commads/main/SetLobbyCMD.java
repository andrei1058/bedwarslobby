package com.andrei1058.bedwarslobby.commads.main;

import com.andrei1058.bedwarslobby.SpigotMain;
import com.andrei1058.bedwarslobby.commads.SubCommand;
import com.andrei1058.bedwarslobby.language.Language;
import com.andrei1058.bedwarslobby.language.Messages;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class SetLobbyCMD extends SubCommand {
    /**
     * Create a new sub-command.
     * Do not forget to add it to a parent.
     *
     * @param name       sub-command name.
     * @param permission sub-command permission, leave empty if no permission is required.
     */
    public SetLobbyCMD(String name, String permission) {
        super(name, permission);
    }

    @Override
    public void execute(CommandSender s, String[] args) {
        if (s instanceof ConsoleCommandSender){

            return;
        }

        Player p = (Player) s;
        if (!hasPermission(p)){
            p.sendMessage(Language.getMsg(p, Messages.COMMAND_NOT_FOUND_OR_INSUFF_PERMS));
            return;
        }

        SpigotMain.config.saveConfigLoc("spawn-location", p.getLocation());
        p.sendMessage("§6 ▪ §7Spawn location saved!");
    }
}

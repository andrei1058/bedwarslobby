package com.andrei1058.bedwarslobby.commads.main;

import com.andrei1058.bedwarslobby.commads.SubCommand;
import com.andrei1058.bedwarslobby.stats.StatsManager;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class StatsCMD extends SubCommand {

    public StatsCMD(String name, String perm) {
        super(name, perm);

    }

    @Override
    public void execute(CommandSender s, String[] args) {
        if (s instanceof ConsoleCommandSender) return;
        Player p = (Player) s;
        if (statsCoolDown.containsKey(p)){
            if (System.currentTimeMillis() - 3000 >= statsCoolDown.get(p)) {
                statsCoolDown.replace(p, System.currentTimeMillis());
            } else {
                //wait 3 seconds
                return;
            }
        } else {
            statsCoolDown.put(p, System.currentTimeMillis());
        }
        StatsManager.openStatsGUI(p);
    }

    private static HashMap<Player, Long> statsCoolDown = new HashMap<>();
}

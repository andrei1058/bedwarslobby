package com.andrei1058.bedwarslobby.commads.main;

import com.andrei1058.bedwarslobby.SpigotMain;
import com.andrei1058.bedwarslobby.misc.Utils;
import com.andrei1058.bedwarslobby.commads.SubCommand;
import com.andrei1058.bedwarslobby.language.Language;
import com.andrei1058.bedwarslobby.language.Messages;
import com.andrei1058.bedwarslobby.levels.internal.PlayerLevel;
import net.md_5.bungee.api.chat.ClickEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LevelCMD extends SubCommand {
    /**
     * Create a sub-command for a bedWars command
     * Make sure you return true or it will say command not found
     *
     * @param name   sub-command name
     * @since 0.6.1 api v6
     */
    public LevelCMD(String name, String permission) {
        super(name, permission);
    }

    @Override
    public void execute(CommandSender s, String[] args) {
        if (args.length == 0) {
            sendSubCommands(s);
            return;
        }

        if (!hasPermission(s)) {
            if (s instanceof Player)
                s.sendMessage(Language.getMsg(((Player) s), Messages.COMMAND_NOT_FOUND_OR_INSUFF_PERMS));
            else
                s.sendMessage(Language.defaultLanguage.m(Messages.COMMAND_NOT_FOUND_OR_INSUFF_PERMS));
            return;
        }

        if (args[0].equalsIgnoreCase("setlevel")) {
            if (args.length != 3) {
                s.sendMessage(ChatColor.GOLD + " ▪ " + ChatColor.GRAY + "Usage: /bw level setLevel §o<player> <level>");
                return;
            }
            Player pl = Bukkit.getPlayer(args[1]);
            if (pl == null) {
                s.sendMessage(ChatColor.RED + " ▪ " + ChatColor.GRAY + "Player not found!");
                return;
            }

            int level;

            try {
                level = Integer.parseInt(args[2]);
            } catch (Exception e) {
                s.sendMessage(ChatColor.RED + "LevelCMD must be an integer!");
                return;
            }

            PlayerLevel pv = PlayerLevel.getLevelByPlayer(pl.getUniqueId());
            if (pv != null) pv.setLevel(level);

            SpigotMain.remoteDatabase.setLevelData(pl.getUniqueId(), level, 0);
            s.sendMessage(ChatColor.GOLD + " ▪ " + ChatColor.GRAY + pl.getName() + " level was set to: " + args[2]);
            s.sendMessage(ChatColor.GOLD + " ▪ " + ChatColor.GRAY + "The player may need to rejoin to see it updated.");
        } else if (args[0].equalsIgnoreCase("givexp")) {
            if (args.length != 3) {
                s.sendMessage(ChatColor.GOLD + " ▪ " + ChatColor.GRAY + "Usage: /bw level giveXp §o<player> <amount>");
                return;
            }
            Player pl = Bukkit.getPlayer(args[1]);
            if (pl == null) {
                s.sendMessage(ChatColor.RED + " ▪ " + ChatColor.GRAY + "Player not found!");
                return;
            }

            int amount;

            try {
                amount = Integer.parseInt(args[2]);
            } catch (Exception e) {
                s.sendMessage(ChatColor.RED + "Amount must be an integer!");
                return;
            }

            PlayerLevel pv = PlayerLevel.getLevelByPlayer(pl.getUniqueId());
            if (pv != null) pv.setXp(amount);

            Object[] data = SpigotMain.remoteDatabase.getLevelData(pl.getUniqueId());
            SpigotMain.remoteDatabase.setLevelData(pl.getUniqueId(), (Integer) data[0], ((Integer) data[1]) + amount);
            s.sendMessage(ChatColor.GOLD + " ▪ " + ChatColor.GRAY + args[2] + " xp was given to: " + pl.getName());
            s.sendMessage(ChatColor.GOLD + " ▪ " + ChatColor.GRAY + "The player may need to rejoin to see it updated.");
        } else {
            sendSubCommands(s);
        }
        return;
    }

    private void sendSubCommands(CommandSender s) {
        if (s instanceof Player) {
            Player p = (Player) s;
            p.spigot().sendMessage(Utils.msgHoverClick("§6 ▪ §7/bw " + getName() + " setLevel §o<player> <level>",
                    "Set a player level.", "/bw " + getName() + " setLevel",
                    ClickEvent.Action.SUGGEST_COMMAND));
            p.spigot().sendMessage(Utils.msgHoverClick("§6 ▪ §7/bw " + getName() + " giveXp §o<player> <amount>",
                    "Give Xp to a player.", "/bw " + getName() + " giveXp",
                    ClickEvent.Action.SUGGEST_COMMAND));
        } else {
            s.sendMessage(ChatColor.GOLD + "bw level setLevel <player> <level>");
            s.sendMessage(ChatColor.GOLD + "bw level giveXp <player> <amount>");
        }
    }
}

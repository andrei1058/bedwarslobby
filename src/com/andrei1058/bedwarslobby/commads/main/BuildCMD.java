package com.andrei1058.bedwarslobby.commads.main;

import com.andrei1058.bedwarslobby.commads.SubCommand;
import com.andrei1058.bedwarslobby.language.Language;
import com.andrei1058.bedwarslobby.language.Messages;
import com.andrei1058.bedwarslobby.listeners.WorldProtection;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.UUID;

public class BuildCMD extends SubCommand {
    /**
     * Create a new sub-command.
     * Do not forget to add it to a parent.
     *
     * @param name       sub-command name.
     * @param permission sub-command permission, leave empty if no permission is required.
     */
    public BuildCMD(String name, String permission) {
        super(name, permission);
    }

    @Override
    public void execute(CommandSender s, String[] args) {
        if (s instanceof ConsoleCommandSender){
            return;
        }

        Player p = (Player) s;
        if (!hasPermission(p)){
            p.sendMessage(Language.getMsg(p, Messages.COMMAND_NOT_FOUND_OR_INSUFF_PERMS));
            return;
        }

        if (new ArrayList<UUID>(WorldProtection.getBuildSession()).contains(p.getUniqueId())){
            WorldProtection.getBuildSession().remove(p.getUniqueId());
            p.sendMessage("§6 ▪ §7You can't place and break blocks anymore!");
        } else {
            WorldProtection.getBuildSession().add(p.getUniqueId());
            p.sendMessage("§6 ▪ §7You can place and break blocks now.");
        }
    }
}

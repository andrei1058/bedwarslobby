package com.andrei1058.bedwarslobby.commads.party;

import com.andrei1058.bedwarslobby.commads.SubCommand;
import com.andrei1058.bedwarslobby.language.Messages;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static com.andrei1058.bedwarslobby.SpigotMain.getParty;
import static com.andrei1058.bedwarslobby.language.Language.getMsg;

public class LeaveCMD extends SubCommand {

 public LeaveCMD(String name, String permission) {
        super(name, permission);
    }

    @Override
    public void execute(CommandSender s, String[] args) {
        if (!(s instanceof Player)) return;
        Player p = (Player) s;
        if (!getParty().hasParty(p)) {
            p.sendMessage(getMsg(p, Messages.COMMAND_PARTY_GENERAL_DENIED_NOT_IN_PARTY));
            return;
        }
        if (getParty().isOwner(p)) {
            p.sendMessage(getMsg(p, Messages.COMMAND_PARTY_LEAVE_DENIED_IS_OWNER_NEEDS_DISBAND));
            return;
        }
        getParty().removeFromParty(p);
    }
}

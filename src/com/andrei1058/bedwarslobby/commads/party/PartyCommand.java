package com.andrei1058.bedwarslobby.commads.party;

import com.andrei1058.bedwarslobby.SpigotMain;
import com.andrei1058.bedwarslobby.commads.ParentCommand;
import com.andrei1058.bedwarslobby.language.Language;
import com.andrei1058.bedwarslobby.language.Messages;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

import static com.andrei1058.bedwarslobby.SpigotMain.getParty;
import static com.andrei1058.bedwarslobby.language.Language.getList;
import static com.andrei1058.bedwarslobby.language.Language.getMsg;

public class PartyCommand extends ParentCommand {

    public PartyCommand(String name) {
        super(name);
        addSubCommand(new InviteCMD("invite", ""));
        addSubCommand(new AcceptCMD("accept", ""));
        addSubCommand(new LeaveCMD("leave", ""));
        addSubCommand(new DisbandCMD("disband", ""));
        addSubCommand(new RemoveCMD("remove", ""));
    }

    //owner, target
    private static HashMap<UUID, UUID> partySessionRequest = new HashMap<>();

    @Override
    public void sendDefaultMessage(CommandSender s) {
        if (s instanceof ConsoleCommandSender) return;
        Player p = (Player) s;
        for (String st : getList(p, Messages.COMMAND_PARTY_HELP)) {
            p.sendMessage(st);
        }
    }

    /**
     * Get list of requests.
     */
    public static HashMap<UUID, UUID> getPartySessionRequest() {
        return partySessionRequest;
    }
}

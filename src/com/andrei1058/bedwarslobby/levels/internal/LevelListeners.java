package com.andrei1058.bedwarslobby.levels.internal;

import com.andrei1058.bedwarslobby.SpigotMain;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.UUID;

public class LevelListeners implements Listener {

    public static LevelListeners instance;

    public LevelListeners() {
        instance = this;
    }

    //create new level data on player join
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        final UUID u = e.getPlayer().getUniqueId();
        Bukkit.getScheduler().runTaskAsynchronously(SpigotMain.getInstance(), () -> {
            if (PlayerLevel.getLevelByPlayer(e.getPlayer().getUniqueId()) != null) return;
            Object[] levelData = SpigotMain.remoteDatabase.getLevelData(u);
            new PlayerLevel(e.getPlayer().getUniqueId(), (Integer)levelData[0], (Integer) levelData[1], (String)levelData[2], (Integer)levelData[3]);
        });
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        final UUID u = e.getPlayer().getUniqueId();
        Bukkit.getScheduler().runTaskAsynchronously(SpigotMain.getInstance(), () -> {
            PlayerLevel pl = PlayerLevel.getLevelByPlayer(u);
            if (pl != null) pl.destroy();
        });
    }
}

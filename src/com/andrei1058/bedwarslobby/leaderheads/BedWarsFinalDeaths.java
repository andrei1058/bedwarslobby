package com.andrei1058.bedwarslobby.leaderheads;

import com.andrei1058.bedwarslobby.SpigotMain;
import com.andrei1058.bedwarslobby.stats.StatsManager;
import me.robin.leaderheads.datacollectors.OnlineDataCollector;
import me.robin.leaderheads.objects.BoardType;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class BedWarsFinalDeaths extends OnlineDataCollector {

    public BedWarsFinalDeaths() {
        super("bw-final-deaths", SpigotMain.getInstance().getName(), BoardType.DEFAULT, "&8BedWars Final Deaths", "openbwfinaldeaths", Arrays.asList(null, null, "&e{amount} final deaths", null));
    }

    @Override
    public Double getScore(Player player) {
        return (double) StatsManager.getStatsCache().getFinalDeaths(player.getUniqueId());
    }
}

package com.andrei1058.bedwarslobby.leaderheads;

import com.andrei1058.bedwarslobby.SpigotMain;
import com.andrei1058.bedwarslobby.stats.StatsManager;
import me.robin.leaderheads.datacollectors.OnlineDataCollector;
import me.robin.leaderheads.objects.BoardType;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class BedWarsWins extends OnlineDataCollector {

    public BedWarsWins() {
        super("bw-wins", SpigotMain.getInstance().getName(), BoardType.DEFAULT, "&8BedWars Wins", "openbwwins", Arrays.asList(null, null, "&e{amount} wins", null));
    }

    @Override
    public Double getScore(Player player) {
        return (double) StatsManager.getStatsCache().getWins(player.getUniqueId());
    }
}

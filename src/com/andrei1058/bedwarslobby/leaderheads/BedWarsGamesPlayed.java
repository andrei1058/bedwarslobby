package com.andrei1058.bedwarslobby.leaderheads;

import com.andrei1058.bedwarslobby.SpigotMain;
import com.andrei1058.bedwarslobby.stats.StatsManager;
import me.robin.leaderheads.datacollectors.OnlineDataCollector;
import me.robin.leaderheads.objects.BoardType;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class BedWarsGamesPlayed extends OnlineDataCollector {

    public BedWarsGamesPlayed() {
        super("bw-games-played", SpigotMain.getInstance().getName(), BoardType.DEFAULT, "&8BedWars Games Played", "openbwgames", Arrays.asList(null, null, "&e{amount} games played", null));
    }

    @Override
    public Double getScore(Player player) {
        return (double) StatsManager.getStatsCache().getGamesPlayed(player.getUniqueId());
    }
}

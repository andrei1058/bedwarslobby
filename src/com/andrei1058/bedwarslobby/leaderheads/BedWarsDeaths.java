package com.andrei1058.bedwarslobby.leaderheads;

import com.andrei1058.bedwarslobby.SpigotMain;
import com.andrei1058.bedwarslobby.stats.StatsManager;
import me.robin.leaderheads.datacollectors.OnlineDataCollector;
import me.robin.leaderheads.objects.BoardType;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class BedWarsDeaths extends OnlineDataCollector {

    public BedWarsDeaths() {
        super("bw-deaths", SpigotMain.getInstance().getName(), BoardType.DEFAULT, "&8BedWars Deaths", "openbwdeaths", Arrays.asList(null, null, "&e{amount} deaths", null));
    }

    @Override
    public Double getScore(Player player) {
        return (double) StatsManager.getStatsCache().getDeaths(player.getUniqueId());
    }
}

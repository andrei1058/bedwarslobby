package com.andrei1058.bedwarslobby.leaderheads;

import com.andrei1058.bedwarslobby.SpigotMain;
import com.andrei1058.bedwarslobby.stats.StatsManager;
import me.robin.leaderheads.datacollectors.OnlineDataCollector;
import me.robin.leaderheads.objects.BoardType;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class BedWarsLosses extends OnlineDataCollector {

    public BedWarsLosses() {
        super("bw-losses", SpigotMain.getInstance().getName(), BoardType.DEFAULT, "&8BedWars Losses", "openbwlosses", Arrays.asList(null, null, "&e{amount} losses", null));
    }

    @Override
    public Double getScore(Player player) {
        return (double) StatsManager.getStatsCache().getLosses(player.getUniqueId());
    }
}

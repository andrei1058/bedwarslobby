package com.andrei1058.bedwarslobby.leaderheads;

import com.andrei1058.bedwarslobby.SpigotMain;
import com.andrei1058.bedwarslobby.stats.StatsManager;
import me.robin.leaderheads.datacollectors.OnlineDataCollector;
import me.robin.leaderheads.objects.BoardType;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class BedWarsBedsDestroyed extends OnlineDataCollector {

    public BedWarsBedsDestroyed() {
        super("bw-beds", SpigotMain.getInstance().getName(), BoardType.DEFAULT, "&8BedWars Beds Destroyed", "openbwbeds", Arrays.asList(null, null, "&e{amount} beds destroyed", null));
    }

    public Double getScore(Player player) {
        return (double) StatsManager.getStatsCache().getBedsDestroyed(player.getUniqueId());
    }
}

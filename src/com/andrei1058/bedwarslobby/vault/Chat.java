package com.andrei1058.bedwarslobby.vault;

import org.bukkit.entity.Player;

public interface Chat {

    String getPrefix(Player p);
    String getSuffix(Player p);
}

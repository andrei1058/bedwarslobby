package com.andrei1058.bedwarslobby.task;

import com.andrei1058.bedwarslobby.SpigotMain;
import com.andrei1058.bedwarslobby.arena.ArenaData;
import org.bukkit.Bukkit;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class LobbySocket {

    private int port;
    private ServerSocket serverSocket;
    public static boolean run = true;

    public LobbySocket(int port) throws IOException {
        this.port = port;
        serverSocket = new ServerSocket(port);

        SpigotMain.getInstance().getLogger().info("Starting lobby socket on: " + Bukkit.getServer().getIp() + ":" + port);
        Bukkit.getScheduler().runTaskAsynchronously(SpigotMain.getInstance(), ()-> {
           while (run) {
               try {
                   Socket socket = serverSocket.accept();
                   Bukkit.getScheduler().runTaskAsynchronously(SpigotMain.getInstance(), new ClientHandler(socket));
               } catch (IOException e) {
                   e.printStackTrace();
               }
           }
        });
    }

    private class ClientHandler implements Runnable {

        private Socket socket;
        private Scanner in;
        private Long lastMessage;

        public ClientHandler(Socket socket){
            this.socket = socket;
            try {
                in = new Scanner(socket.getInputStream());
            } catch (IOException e) {
            }
            this.lastMessage = System.currentTimeMillis()+8000;
        }

        @Override
        public void run() {
            while (run){
                if (in.hasNext()){
                    String[] i = in.nextLine().split("\\.");
                    if (i.length == 7){
                        ArenaData ad = ArenaData.getArenaByName(i[0]);
                        if (ad == null){
                            ad = new ArenaData(i[0]);
                        }
                        ad.update(i[1], i[2], Integer.valueOf(i[3]), Integer.valueOf(i[4]), i[5], Integer.valueOf(i[6]));
                    } else {
                        if (i[0].equalsIgnoreCase("stop")){
                            in.close();
                            try {
                                socket.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            break;
                        }
                    }
                } else {
                    if (lastMessage < System.currentTimeMillis()){
                        in.close();
                        try {
                            socket.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    }
                }
            }
        }
    }
}

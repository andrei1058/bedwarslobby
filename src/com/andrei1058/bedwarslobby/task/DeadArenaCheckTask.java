package com.andrei1058.bedwarslobby.task;

import com.andrei1058.bedwarslobby.SpigotMain;
import com.andrei1058.bedwarslobby.arena.ArenaData;
import com.andrei1058.bedwarslobby.arena.ArenaStatus;

public class DeadArenaCheckTask implements Runnable {

    private Long waitForRestarting = 30000L, setOffline = 8000L;

    public DeadArenaCheckTask() {
        waitForRestarting = Long.valueOf(SpigotMain.config.getInt("set-offline-after.wait-for-restart") * 1000);
        setOffline = Long.valueOf(SpigotMain.config.getInt("set-offline-after.no-response") * 1000);
    }

    @Override
    public void run() {
        for (ArenaData ad : ArenaData.getArenaByIdentifier().values()) {
            if (System.currentTimeMillis() - ad.getLastUpdate() > setOffline) {
                if (ad.getStatus() == ArenaStatus.RESTARTING && System.currentTimeMillis() - ad.getLastUpdate() > waitForRestarting) {
                    continue;
                }
                ad.setArenaOffline();
                ArenaData.getArenaByIdentifier().remove(ad.getServerName());
            }
        }
    }
}



package com.andrei1058.bedwarslobby.language;

import com.andrei1058.bedwarslobby.SpigotMain;
import com.andrei1058.bedwarslobby.misc.Utils;
import com.andrei1058.bedwarslobby.config.ConfigPath;
import com.andrei1058.bedwarslobby.events.PlayerLangChangeEvent;
import com.andrei1058.bedwarslobby.scoreboard.SBoard;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Language {

    private YamlConfiguration yml;
    private File config;
    private String iso, prefixColor = "", langName = "English";
    private static HashMap<Player, Language> langByPlayer = new HashMap<>();
    private static List<Language> languages = new ArrayList<>();

    public static Language defaultLanguage;

    public Language(String iso) {
        this.iso = iso;
        File d = new File("plugins/" + SpigotMain.getInstance().getDescription().getName() + "/Languages");
        if (!d.exists()) d.mkdir();
        config = new File(d.toPath() + "/messages_" + iso + ".yml");
        if (!config.exists()) {
            try {
                config.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            SpigotMain.getInstance().getLogger().info("Creating " + d.toPath() + "/messages_" + iso + ".yml");
        }
        yml = YamlConfiguration.loadConfiguration(config);
        languages.add(this);
    }

    public static void setupLang(Language lbj) {
        YamlConfiguration yml = lbj.yml;
        yml.options().copyDefaults(true);
        switch (lbj.iso) {
            default:
                new English(lbj, yml);
                break;
        }

        lbj.save();
        lbj.langName = lbj.m("name");
        lbj.prefixColor = ChatColor.translateAlternateColorCodes('&', yml.getString(Messages.PREFIX));
    }

    public static List<String> getScoreboard(Player p, String path, String alternative) {
        Language language = getPlayerLanguage(p);
        if (language.exists(path)) {
            return language.l(path);
        }
        return language.l(alternative);
    }

    public void set(String path, Object value) {
        yml.set(path, value);
        save();
    }

    public String getLangName() {
        return langName;
    }

    public void save() {
        try {
            yml.save(config);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getMsg(Player p, String path) {
        return langByPlayer.getOrDefault(p, defaultLanguage).m(path);
    }

    public static Language getPlayerLanguage(Player p) {
        return langByPlayer.getOrDefault(p, defaultLanguage);
    }

    public boolean exists(String path) {
        return yml.get(path) != null;
    }

    public static List<String> getList(Player p, String path) {
        return langByPlayer.getOrDefault(p, defaultLanguage).l(path);
    }

    public static void saveIfNotExists(String path, Object data) {
        for (Language l : languages) {
            if (l.yml.get(path) == null) {
                l.set(path, data);
            }
        }
    }

    public String m(String path) {
        return ChatColor.translateAlternateColorCodes('&', yml.getString(path).replace("{prefix}", prefixColor));
    }

    public List<String> l(String path) {
        return yml.getStringList(path).stream().map(s -> s.replace("&", "§")).collect(Collectors.toList());
    }

    public static HashMap<Player, Language> getLangByPlayer() {
        return langByPlayer;
    }

    public static boolean isLanguageExist(String iso) {
        for (Language l : languages) {
            if (l.iso.equalsIgnoreCase(iso)) {
                return true;
            }
        }
        return false;
    }

    public static Language getLang(String iso) {
        for (Language l : languages) {
            if (l.iso.equalsIgnoreCase(iso)) {
                return l;
            }
        }
        return defaultLanguage;
    }

    public void reload() {
        this.yml = YamlConfiguration.loadConfiguration(config);
    }

    public String getIso() {
        return iso;
    }

    public static List<Language> getLanguages() {
        return languages;
    }

    public static void setupCustomStatsMessages() {
        for (Language l : getLanguages()) {
            /* save messages for stats gui items if custom items added */
            for (String item : SpigotMain.config.getYml().getConfigurationSection(ConfigPath.GENERAL_CONFIGURATION_STATS_PATH).getKeys(false)) {
                if (ConfigPath.GENERAL_CONFIGURATION_STATS_GUI_SIZE.contains(item)) continue;
                l.yml.addDefault(Messages.PLAYER_STATS_GUI_PATH + "-" + item + "-name", "Name not set");
                l.yml.addDefault(Messages.PLAYER_STATS_GUI_PATH + "-" + item + "-lore", Collections.singletonList("lore not set"));
            }
            l.save();
        }
    }

    public void addDefaultStatsMsg(YamlConfiguration yml, String path, String name, String... lore) {
        yml.addDefault(Messages.PLAYER_STATS_GUI_PATH + "-" + path + "-name", name);
        yml.addDefault(Messages.PLAYER_STATS_GUI_PATH + "-" + path + "-lore", lore);
    }

    /**
     * Create missing name/ lore for items: multi arena lobby, watiting, spectating
     */
    public static void addDefaultMessagesCommandItems(Language language) {
        YamlConfiguration yml = language.yml;
        if (SpigotMain.config.getYml().get(ConfigPath.GENERAL_CONFIGURATION_LOBBY_ITEMS_PATH) != null) {
            for (String item : SpigotMain.config.getYml().getConfigurationSection(ConfigPath.GENERAL_CONFIGURATION_LOBBY_ITEMS_PATH).getKeys(false)) {
                String p1 = Messages.GENERAL_CONFIGURATION_LOBBY_ITEMS_NAME.replace("%path%", item);
                String p2 = Messages.GENERAL_CONFIGURATION_LOBBY_ITEMS_LORE.replace("%path%", item);
                yml.addDefault(p1, "&cName not set at: &f" + p1);
                yml.addDefault(p2, Arrays.asList("&cLore not set at:", " &f" + p2));
            }
        }
        yml.options().copyDefaults(true);
        language.save();
    }

    /**
     * Set the server default language.
     */
    public static void setDefaultLanguage(Language defaultLanguage) {
        Language.defaultLanguage = defaultLanguage;
    }

    /**
     * Change a player language and refresh
     * scoreboard and custom join items.
     */
    public static void setPlayerLanguage(Player p, String iso, boolean onLogin) {

        if (onLogin){
            if (Language.defaultLanguage.getIso().equalsIgnoreCase(iso)) return;
        }

        Language newLang = Language.getLang(iso);

        if (!onLogin) {
            Language oldLang = Language.getLangByPlayer().containsKey(p) ? Language.getPlayerLanguage(p) : Language.getLanguages().get(0);
            PlayerLangChangeEvent e = new PlayerLangChangeEvent(p, oldLang, newLang);
            Bukkit.getPluginManager().callEvent(e);
            if (e.isCancelled()) return;
        }

        if (Language.getLangByPlayer().containsKey(p)) {
            Language.getLangByPlayer().replace(p, newLang);
        } else {
            Language.getLangByPlayer().put(p, newLang);
        }

        if (!onLogin) {
            SpigotMain.remoteDatabase.setLanguage(p.getUniqueId(), iso);
            Utils.sendJoinItems(p);
            new SBoard(p);
        }
    }
}

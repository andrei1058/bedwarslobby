package com.andrei1058.bedwarslobby.arena;

import com.andrei1058.bedwarslobby.SpigotMain;
import com.andrei1058.bedwarslobby.citizens.JoinNPC;
import com.andrei1058.bedwarslobby.language.Language;
import com.andrei1058.bedwarslobby.language.Messages;
import org.bukkit.*;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class ArenaData implements Comparable {

    private static ConcurrentHashMap<String, ArenaData> arenaByIdentifier = new ConcurrentHashMap<>();
    private static int totalPlaying = 0;

    private String serverName = "", arenaName = "", arenaGroup = "Default", bungeeName = "notSet";
    private int currentPlayers, maxPlayers, maxInTeam;
    private ArenaStatus status = ArenaStatus.RESTARTING;
    private List<BlockState> signs = new ArrayList<>();
    private Long lastUpdate = 0L;

    /**
     * Chache new info about an arena.
     */
    public ArenaData(String serverName) {
        this.serverName = serverName;
        if (SpigotMain.config.getYml().get("bungee-name." + serverName.replace(":", "-")) == null) {
            SpigotMain.config.set("bungee-name." + serverName.replace(":", "-"), "nameNotSet");
            for (OfflinePlayer p : Bukkit.getOperators()) {
                if (p.isOnline()) {
                    Bukkit.getPlayer(p.getUniqueId()).sendMessage(ChatColor.GREEN + "A new bedwars arena was added to the config. Please configure its bungeecord name and restart.");
                }
            }
            SpigotMain.getInstance().getLogger().warning("A new bedwars arena was added to the config. Please configure its bungeecord name and restart.");

        } else {
            bungeeName = SpigotMain.config.getString("bungee-name." + serverName.replace(":", "-"));
        }
        registerSigns();
        arenaByIdentifier.put(serverName, this);
    }

    public void addSign(Location loc) {
        if (loc == null) return;
        if (loc.getBlock().getType() == Material.SIGN || loc.getBlock().getType() == Material.WALL_SIGN) {
            signs.add(loc.getBlock().getState());
            refreshSigns();
        }
    }

    /**
     * Refresh placed signs for this arena.
     */
    public void refreshSigns() {
        for (BlockState b : signs) {
            Sign s = (Sign) b;
            int line = 0;
            for (String string : SpigotMain.config.l("join-signs.format." + getStatus().toString().toLowerCase())) {
                s.setLine(line, string.replace("[on]", String.valueOf(getCurrentPlayers())).replace("[max]",
                        String.valueOf(getMaxPlayers())).replace("[arena]", getArenaName()).replace("[status]",
                        getDisplayStatus(null)));
                line++;
            }

            try {
                b.update(true);
            } catch (Exception e) {
            }

            String path = "join-signs.status-block." + getStatus().toString().toLowerCase() + ".material",
                    data = "join-signs.status-block." + getStatus().toString().toLowerCase() + ".data";

            Bukkit.getScheduler().runTask(SpigotMain.getInstance(), () ->
            {
                s.getLocation().getBlock().getRelative(((org.bukkit.material.Sign) s.getData()).getAttachedFace()).setType(Material.valueOf(SpigotMain.config.getString(path)));
                SpigotMain.getVersionSupport().setBlockData(s.getBlock().getRelative(((org.bukkit.material.Sign) s.getData()).getAttachedFace()), (byte) SpigotMain.config.getInt(data));

            });
        }
    }

    /**
     * Declare arena as offline.
     */
    public void setArenaOffline() {
        status = ArenaStatus.OFFLINE;
        refreshSigns();
        for (Player p : ArenaGUI.getRefresh().keySet()) {
            ArenaGUI.refreshInv(p, ArenaGUI.getRefresh().get(p));
        }
    }

    public void update(String arenaName, String status, int currentPlayers, int maxPlayers, String arenaGroup, int maxInTeam) {
        setStatus(status);
        setCurrentPlayers(currentPlayers);
        setMaxPlayers(maxPlayers);
        setArenaGroup(arenaGroup);
        setArenaName(arenaName);
        setMaxInTeam(maxInTeam);
        lastUpdate = System.currentTimeMillis();
        Bukkit.getScheduler().runTask(SpigotMain.getInstance(), () -> {
            refreshSigns();
            for (Player p : ArenaGUI.getRefresh().keySet()) {
                ArenaGUI.refreshInv(p, ArenaGUI.getRefresh().get(p));
            }
        });
        if (JoinNPC.isCitizensSupport()) JoinNPC.updateNPCs(getArenaGroup());
    }


    public int getCurrentPlayers() {
        return currentPlayers;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public static ConcurrentHashMap<String, ArenaData> getArenaByIdentifier() {
        return arenaByIdentifier;
    }

    public String getArenaName() {
        return arenaName;
    }

    public String getServerName() {
        return serverName;
    }

    public String getArenaGroup() {
        return arenaGroup;
    }

    public ArenaStatus getStatus() {
        return status;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public void setArenaName(String arenaName) {
        this.arenaName = arenaName;
    }

    public void setMaxInTeam(int maxInTeam) {
        this.maxInTeam = maxInTeam;
    }

    public int getMaxInTeam() {
        return maxInTeam;
    }

    public void setCurrentPlayers(int currentPlayers) {
        this.currentPlayers = currentPlayers;
        for (ArenaData ad : arenaByIdentifier.values()) {
            totalPlaying += ad.getCurrentPlayers();
        }
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public void setArenaGroup(String arenaGroup) {
        this.arenaGroup = arenaGroup;
    }

    /**
     * Get arena status translated.
     * Use null to get in default language.
     */
    public String getDisplayStatus(Player p) {
        String status = Messages.ARENA_STATUS_RESTARTING_NAME;
        switch (getStatus()) {
            case WAITING:
                status = Messages.ARENA_STATUS_WAITING_NAME;
                break;
            case PLAYING:
                status = Messages.ARENA_STATUS_PLAYING_NAME;
                break;
            case STARTING:
                status = Messages.ARENA_STATUS_STARTING_NAME;
                break;
            case OFFLINE:
                status = Messages.ARENA_STATUS_OFFLINE_NAME;
                break;
        }
        return p == null ? Language.defaultLanguage.m(status).replace("{full}", getCurrentPlayers() == getMaxPlayers() ? Language.defaultLanguage.m(Messages.MEANING_FULL) : "")
                : Language.getMsg(p, status).replace("{full}", getCurrentPlayers() == getMaxPlayers() ? Language.getMsg(p, Messages.MEANING_FULL) : "");
    }

    public List<BlockState> getSigns() {
        return signs;
    }

    public void setStatus(String status) {
        this.status = ArenaStatus.valueOf(status.toUpperCase());
    }

    /**
     * Get arena data for tenical server name.
     * <p>
     * Check getArenaByBungeeName.
     */
    public static ArenaData getArenaByName(String serverName) {
        return getArenaByIdentifier().getOrDefault(serverName, null);
    }

    /**
     * Get arena data for given server.
     */
    public static ArenaData getArenaByBungeeName(String bungeeName) {
        for (ArenaData arenaData : getArenaByIdentifier().values()) {
            if (arenaData.getBungeeName().equals(bungeeName)) return arenaData;
        }
        return null;
    }

    /**
     * Load arena signs.
     */
    private void registerSigns() {
        if (SpigotMain.config.getYml().get("join-signs.locations") != null) {
            for (String st : SpigotMain.config.getYml().getStringList("join-signs.locations")) {
                String[] data = st.split(",");
                if (data[0].equals(getBungeeName()))
                    addSign(new Location(Bukkit.getWorld(data[6]), Double.valueOf(data[1]), Double.valueOf(data[2]), Double.valueOf(data[3])));
            }
        }
    }

    /**
     * Get the bungeecord name for this arena.
     */
    public String getBungeeName() {
        return bungeeName;
    }

    /**
     * Last time the arena was updated.
     */
    public Long getLastUpdate() {
        return lastUpdate;
    }

    @Override
    public int compareTo(Object o) {
        ArenaData a2 = (ArenaData) o;
        if (getStatus() == ArenaStatus.STARTING && a2.getStatus() == ArenaStatus.STARTING) {
            if (getCurrentPlayers() > a2.getCurrentPlayers()) {
                return -1;
            }
            if (getCurrentPlayers() == a2.getCurrentPlayers()) {
                return 0;
            } else return 1;
        } else if (getStatus() == ArenaStatus.STARTING && a2.getStatus() != ArenaStatus.STARTING) {
            return -1;
        } else if (a2.getStatus() == ArenaStatus.STARTING && getStatus() != ArenaStatus.STARTING) {
            return 1;
        } else if (getStatus() == ArenaStatus.WAITING && a2.getStatus() == ArenaStatus.WAITING) {
            if (getCurrentPlayers() > a2.getCurrentPlayers()) {
                return -1;
            }
            if (getCurrentPlayers() == a2.getCurrentPlayers()) {
                return 0;
            } else return 1;
        } else if (getStatus() == ArenaStatus.WAITING && a2.getStatus() != ArenaStatus.WAITING) {
            return -1;
        } else if (a2.getStatus() == ArenaStatus.WAITING && getStatus() != ArenaStatus.WAITING) {
            return 1;
        } else if (getStatus() == ArenaStatus.PLAYING && a2.getStatus() == ArenaStatus.PLAYING) {
            return 0;
        } else if (getStatus() == ArenaStatus.PLAYING && a2.getStatus() != ArenaStatus.PLAYING) {
            return -1;
        } else return 1;
    }

    /**
     * Get the amount of players in game.
     */
    public static int getTotalPlaying() {
        return totalPlaying;
    }

    /**
     * Get amount of players in an arena gorup.
     */
    public static int getPlayers(String group) {
        int i = 0;
        for (ArenaData a : ArenaData.getArenaByIdentifier().values()) {
            if (a.getArenaGroup().equalsIgnoreCase(group)) i += a.getCurrentPlayers();
        }
        return i;
    }

    /** Check if arena group exists.*/
    public static boolean hasGroup(String arenaGroup){
        for (ArenaData ad : arenaByIdentifier.values()){
            if (ad.getArenaGroup().equalsIgnoreCase(arenaGroup)) return true;
        }
        return false;
    }
}

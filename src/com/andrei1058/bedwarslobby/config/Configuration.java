package com.andrei1058.bedwarslobby.config;

import com.andrei1058.bedwarslobby.SpigotMain;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Configuration {

    private YamlConfiguration yml;
    private File config;
    private boolean firstTime = false;

    public static final String GENERAL_CONFIGURATION_ARENA_SELECTOR_PATH = "arena-gui";
    public static final String GENERAL_CONFIGURATION_ARENA_SELECTOR_SETTINGS_SIZE = GENERAL_CONFIGURATION_ARENA_SELECTOR_PATH + ".settings.inv-size";
    public static final String GENERAL_CONFIGURATION_ARENA_SELECTOR_SETTINGS_SHOW_PLAYING = GENERAL_CONFIGURATION_ARENA_SELECTOR_PATH + ".settings.show-playing";
    public static final String GENERAL_CONFIGURATION_ARENA_SELECTOR_SETTINGS_USE_SLOTS = GENERAL_CONFIGURATION_ARENA_SELECTOR_PATH + ".settings.use-slots";
    public static final String GENERAL_CONFIGURATION_ARENA_SELECTOR_STATUS_MATERIAL = GENERAL_CONFIGURATION_ARENA_SELECTOR_PATH + ".%path%.material";
    public static final String GENERAL_CONFIGURATION_ARENA_SELECTOR_STATUS_DATA = GENERAL_CONFIGURATION_ARENA_SELECTOR_PATH + ".%path%.data";
    public static final String GENERAL_CONFIGURATION_ARENA_SELECTOR_STATUS_ENCHANTED = GENERAL_CONFIGURATION_ARENA_SELECTOR_PATH + ".%path%.enchanted";

    public Configuration(String name, String dir) {
        File d = new File(dir);
        if (!d.exists()) {
            d.mkdir();
            firstTime = true;
        }
        config = new File(dir + "/" + name + ".yml");
        if (!config.exists()) {
            firstTime = true;
            try {
                config.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            SpigotMain.getInstance().getLogger().info("Creating " + dir + "/" + name + ".yml");
        }
        yml = YamlConfiguration.loadConfiguration(config);
        setup();
    }

    public void setup(){

        yml.options().copyDefaults(true);
        yml.options().header("Documentation here: https://gitlab.com/andrei1058/bedwarslobby/wikis/home");

        yml.addDefault("default-language", "en");
        yml.addDefault("socket-port", 2019);
        yml.addDefault("protect-world", true);
        yml.addDefault("protect-players", true);
        yml.addDefault("spawn-on-join", true);
        yml.addDefault("format-chat", true);
        yml.addDefault("disable-join-message", true);
        yml.addDefault("clear-armor-on-join", true);
        yml.addDefault("set-offline-after.no-response", 8);
        yml.addDefault("set-offline-after.wait-for-restart", 30);
        yml.addDefault("bungee-name.i-p-port", "arena1");
        yml.addDefault("spawn-location", "null");
        yml.addDefault("levels-settings.default-name", "&7[{number}✩] ");
        yml.addDefault("levels-settings.progress-bar-symbol", "■");
        yml.addDefault("levels-settings.progress-bar-unlocked-color", "&b");
        yml.addDefault("levels-settings.progress-bar-locked-color", "&7");
        yml.addDefault("levels-settings.progress-bar-format", "&8 [{progress}&8]");
        yml.addDefault(ConfigPath.GENERAL_CONFIGURATION_DISABLED_LANGUAGES, Arrays.asList("your language iso here"));
        yml.addDefault(ConfigPath.GENERAL_CONFIGURATION_LOBBY_SCOREBOARD, true);

        yml.addDefault("database.host", "localhost");
        yml.addDefault("database.port", 3306);
        yml.addDefault("database.database", "bedwars1058");
        yml.addDefault("database.user", "root");
        yml.addDefault("database.pass", "cheez");
        yml.addDefault("database.ssl", false);

        saveCommandItem("player-stats", "bw stats", false, SpigotMain.getVersionSupport().getForCurrentVersion("SKULL_ITEM", "SKULL_ITEM", "PLAYER_HEAD"), 3, 0);
        saveCommandItem("arena-selector", "bw gui", false, "CHEST", 5, 4);
        saveCommandItem("leave-item", "bw leave", false, SpigotMain.getVersionSupport().getForCurrentVersion("BED", "BED", "RED_BED"), 0, 8);

        yml.addDefault(GENERAL_CONFIGURATION_ARENA_SELECTOR_SETTINGS_SIZE, 27);
        yml.addDefault(GENERAL_CONFIGURATION_ARENA_SELECTOR_SETTINGS_SHOW_PLAYING, true);
        yml.addDefault(GENERAL_CONFIGURATION_ARENA_SELECTOR_SETTINGS_USE_SLOTS, "10,11,12,13,14,15,16");
        yml.addDefault(GENERAL_CONFIGURATION_ARENA_SELECTOR_STATUS_MATERIAL.replace("%path%", "waiting"), SpigotMain.getVersionSupport().getForCurrentVersion("STAINED_GLASS_PANE", "CONCRETE", "LIME_CONCRETE"));
        yml.addDefault(GENERAL_CONFIGURATION_ARENA_SELECTOR_STATUS_DATA.replace("%path%", "waiting"), 5);
        yml.addDefault(GENERAL_CONFIGURATION_ARENA_SELECTOR_STATUS_ENCHANTED.replace("%path%", "waiting"), false);
        yml.addDefault(GENERAL_CONFIGURATION_ARENA_SELECTOR_STATUS_MATERIAL.replace("%path%", "starting"), SpigotMain.getVersionSupport().getForCurrentVersion("STAINED_GLASS_PANE", "CONCRETE", "YELLOW_CONCRETE"));
        yml.addDefault(GENERAL_CONFIGURATION_ARENA_SELECTOR_STATUS_DATA.replace("%path%", "starting"), 4);
        yml.addDefault(GENERAL_CONFIGURATION_ARENA_SELECTOR_STATUS_ENCHANTED.replace("%path%", "starting"), true);
        yml.addDefault(GENERAL_CONFIGURATION_ARENA_SELECTOR_STATUS_MATERIAL.replace("%path%", "playing"), SpigotMain.getVersionSupport().getForCurrentVersion("STAINED_GLASS_PANE", "CONCRETE", "RED_CONCRETE"));
        yml.addDefault(GENERAL_CONFIGURATION_ARENA_SELECTOR_STATUS_DATA.replace("%path%", "playing"), 14);
        yml.addDefault(GENERAL_CONFIGURATION_ARENA_SELECTOR_STATUS_ENCHANTED.replace("%path%", "playing"), false);
        yml.addDefault(GENERAL_CONFIGURATION_ARENA_SELECTOR_STATUS_MATERIAL.replace("%path%", "skipped-slot"), SpigotMain.getVersionSupport().getForCurrentVersion("STAINED_GLASS_PANE", "STAINED_GLASS_PANE", "BLACK_STAINED_GLASS_PANE"));
        yml.addDefault(GENERAL_CONFIGURATION_ARENA_SELECTOR_STATUS_DATA.replace("%path%", "skipped-slot"), 15);
        yml.addDefault(GENERAL_CONFIGURATION_ARENA_SELECTOR_STATUS_ENCHANTED.replace("%path%", "skipped-slot"), false);

        /* default stats GUI items */
        yml.addDefault(ConfigPath.GENERAL_CONFIGURATION_STATS_GUI_SIZE, 27);
        if (isFirstTime()) {
            addDefaultStatsItem(yml, 10, Material.DIAMOND, 0, "wins");
            addDefaultStatsItem(yml, 11, Material.REDSTONE, 0, "losses");
            addDefaultStatsItem(yml, 12, Material.IRON_SWORD, 0, "kills");
            addDefaultStatsItem(yml, 13, Material.valueOf(SpigotMain.getVersionSupport().getForCurrentVersion("SKULL_ITEM", "SKULL_ITEM", "SKELETON_SKULL")), 0, "deaths");
            addDefaultStatsItem(yml, 14, Material.DIAMOND_SWORD, 0, "final-kills");
            addDefaultStatsItem(yml, 15, Material.valueOf(SpigotMain.getVersionSupport().getForCurrentVersion("SKULL_ITEM", "SKULL_ITEM", "SKELETON_SKULL")), 1, "final-deaths");
            addDefaultStatsItem(yml, 16, Material.valueOf(SpigotMain.getVersionSupport().getForCurrentVersion("BED", "BED", "RED_BED")), 0, "beds-destroyed");
            addDefaultStatsItem(yml, 21, Material.valueOf(SpigotMain.getVersionSupport().getForCurrentVersion("STAINED_GLASS_PANE", "STAINED_GLASS_PANE", "BLACK_STAINED_GLASS_PANE")), 0, "first-play");
            addDefaultStatsItem(yml, 22, Material.CHEST, 0, "games-played");
            addDefaultStatsItem(yml, 23, Material.valueOf(SpigotMain.getVersionSupport().getForCurrentVersion("STAINED_GLASS_PANE", "STAINED_GLASS_PANE", "BLACK_STAINED_GLASS_PANE")), 0, "last-play");
        }

        yml.addDefault("join-signs.format.waiting", Arrays.asList("&a[arena]", "", "&2[on]&9/&2[max]", "[status]"));
        yml.addDefault("join-signs.format.starting", Arrays.asList("&a[arena]", "", "&2[on]&9/&2[max]", "[status]"));
        yml.addDefault("join-signs.format.playing", Arrays.asList("&a[arena]", "", "&2[on]&9/&2[max]", "[status]"));
        yml.addDefault("join-signs.format.restarting", Arrays.asList("&a[arena]", "", "&2[on]&9/&2[max]", "[status]"));
        yml.addDefault("join-signs.format.offline", Arrays.asList("&a[arena]", "", "0/0", "&1Offline"));
        yml.addDefault("join-signs.status-block.waiting.material", SpigotMain.getVersionSupport().getForCurrentVersion("STAINED_CLAY", "STAINED_CLAY", "GREEN_CONCRETE"));
        yml.addDefault("join-signs.status-block.waiting.data", 5);
        yml.addDefault("join-signs.status-block.starting.material", SpigotMain.getVersionSupport().getForCurrentVersion("STAINED_CLAY", "STAINED_CLAY", "YELLOW_CONCRETE"));
        yml.addDefault("join-signs.status-block.starting.data", 14);
        yml.addDefault("join-signs.status-block.playing.material", SpigotMain.getVersionSupport().getForCurrentVersion("STAINED_CLAY", "STAINED_CLAY", "RED_CONCRETE"));
        yml.addDefault("join-signs.status-block.playing.data", 4);
        yml.addDefault("join-signs.status-block.restarting.material", SpigotMain.getVersionSupport().getForCurrentVersion("STAINED_CLAY", "STAINED_CLAY", "RED_CONCRETE"));
        yml.addDefault("join-signs.status-block.restarting.data", 4);
        yml.addDefault("join-signs.status-block.offline.material", SpigotMain.getVersionSupport().getForCurrentVersion("STAINED_CLAY", "STAINED_CLAY", "RED_CONCRETE"));
        yml.addDefault("join-signs.status-block.offline.data", 4);
        if (yml.getStringList("join-signs.format.waiting").size() > 4) {
            yml.set("join-signs.format.waiting", yml.getStringList("join-signs.format.waiting").subList(0, 3));
        }
        if (yml.getStringList("join-signs.format.starting").size() > 4) {
            yml.set("join-signs.format.starting", yml.getStringList("join-signs.format.starting").subList(0, 3));
        }
        if (yml.getStringList("join-signs.format.playing").size() > 4) {
            yml.set("join-signs.format.playing", yml.getStringList("join-signs.format.playing").subList(0, 3));
        }
        if (yml.getStringList("join-signs.format.restarting").size() > 4) {
            yml.set("join-signs.format.restarting", yml.getStringList("join-signs.format.restarting").subList(0, 3));
        }
        if (yml.getStringList("join-signs.format.offline").size() > 4) {
            yml.set("join-signs.format.offline", yml.getStringList("join-signs.format.offline").subList(0, 3));
        }
        save();
    }

    public void reload() {
        yml = YamlConfiguration.loadConfiguration(config);
    }

    public String stringLocationConfigFormat(Location loc) {
        return Double.valueOf(loc.getX()) + "," + Double.valueOf(loc.getY()) + "," + Double.valueOf(loc.getZ()) + "," + Double.valueOf(loc.getYaw()) + "," + Double.valueOf(loc.getPitch()) + "," + loc.getWorld().getName();
    }

    public void saveConfigLoc(String path, Location loc) {
        String data = Double.valueOf(loc.getX()) + "," + Double.valueOf(loc.getY()) + "," + Double.valueOf(loc.getZ()) + "," + Double.valueOf(loc.getYaw()) + "," + Double.valueOf(loc.getPitch()) + "," + loc.getWorld().getName();
        yml.set(path, data);
        save();
    }

    public String getConfigLoc(Location loc) {
        return loc.getX() + "," + loc.getY() + "," + loc.getZ() + "," + loc.getYaw() + "," + loc.getPitch() + "," + loc.getWorld().getName();
    }

    public Location getConfigLoc(String path) {
        String d = yml.getString(path);
        if (d == null) return null;
        String[] data = d.replace("[", "").replace("]", "").split(",");
        if (data.length != 6) return null;
        return new Location(Bukkit.getWorld(data[5]), Double.valueOf(data[0]), Double.valueOf(data[1]), Double.valueOf(data[2]), Float.valueOf(data[3]), Float.valueOf(data[4]));
    }

    public void set(String path, Object value) {
        yml.set(path, value);
        save();
    }
    public List<String> l(String path) {
        return yml.getStringList(path).stream().map(s -> s.replace("&", "§")).collect(Collectors.toList());
    }

    public String m(String path){
        return yml.getString(path).replace("&", "§");
    }

    public YamlConfiguration getYml() {
        return yml;
    }

    public void save() {
        try {
            yml.save(config);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public boolean getBoolean(String path) {
        return yml.getBoolean(path);
    }

    public int getInt(String path) {
        return yml.getInt(path);
    }

    public String getString(String path) {
        return yml.getString(path);
    }

    public boolean isFirstTime() {
        return firstTime;
    }

    public void saveCommandItem(String path, String cmd, boolean enchanted, String material, int data, int slot){
        if (isFirstTime()){
            yml.addDefault(ConfigPath.GENERAL_CONFIGURATION_LOBBY_ITEMS_COMMAND.replace("%path%", path), cmd);
            yml.addDefault(ConfigPath.GENERAL_CONFIGURATION_LOBBY_ITEMS_MATERIAL.replace("%path%", path), material);
            yml.addDefault(ConfigPath.GENERAL_CONFIGURATION_LOBBY_ITEMS_DATA.replace("%path%", path), data);
            yml.addDefault(ConfigPath.GENERAL_CONFIGURATION_LOBBY_ITEMS_SLOT.replace("%path%", path), slot);
            yml.addDefault(ConfigPath.GENERAL_CONFIGURATION_LOBBY_ITEMS_ENCHANTED.replace("%path%", path), enchanted);
            yml.options().copyDefaults(true);
            save();
        }
    }

    /**
     * Add default stats gui item
     */
    public static void addDefaultStatsItem(YamlConfiguration yml, int slot, Material itemstack, int data, String path) {
        yml.addDefault(ConfigPath.GENERAL_CONFIGURATION_STATS_ITEMS_MATERIAL.replace("%path%", path), itemstack.toString());
        yml.addDefault(ConfigPath.GENERAL_CONFIGURATION_STATS_ITEMS_DATA.replace("%path%", path), data);
        yml.addDefault(ConfigPath.GENERAL_CONFIGURATION_STATS_ITEMS_SLOT.replace("%path%", path), slot);
    }

    public void setFirstTime(boolean firstTime) {
        this.firstTime = firstTime;
    }
}

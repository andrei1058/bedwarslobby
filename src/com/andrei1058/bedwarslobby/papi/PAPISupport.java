package com.andrei1058.bedwarslobby.papi;

import com.andrei1058.bedwarslobby.SpigotMain;
import com.andrei1058.bedwarslobby.arena.ArenaData;
import com.andrei1058.bedwarslobby.language.Language;
import com.andrei1058.bedwarslobby.language.Messages;
import com.andrei1058.bedwarslobby.stats.StatsManager;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.entity.Player;

import java.text.SimpleDateFormat;

import static com.andrei1058.bedwarslobby.language.Language.getMsg;

public class PAPISupport extends PlaceholderExpansion {

    @Override
    public String getIdentifier() {
        return "bw1058";
    }

    @Override
    public String getAuthor() {
        return "andrei1058";
    }

    @Override
    public String getVersion() {
        return SpigotMain.getInstance().getDescription().getVersion();
    }

    @Override
    public String onPlaceholderRequest(Player p, String s) {

        if (s.contains("arena_status_")){
            ArenaData a = ArenaData.getArenaByBungeeName(s.replace("arena_status_", ""));
            if (a == null){
                return "NULL";
            }
            return a.getDisplayStatus(p);
        }

        if (s.contains("arena_count_")){
            ArenaData a = ArenaData.getArenaByBungeeName(s.replace("arena_status_", ""));
            if (a == null){
                return "0";
            }
            return String.valueOf(a.getCurrentPlayers());
        }

        String replay = "";
        switch (s) {
            case "stats_firstplay":
                replay = new SimpleDateFormat(getMsg(p, Messages.FORMATTING_STATS_DATE_FORMAT)).format(StatsManager.getStatsCache().getFirstPlay(p.getUniqueId()));
                break;
            case "stats_lastplay":
                replay = new SimpleDateFormat(getMsg(p, Messages.FORMATTING_STATS_DATE_FORMAT)).format(StatsManager.getStatsCache().getLastPlay(p.getUniqueId()));
                break;
            case "stats_kills":
                replay = String.valueOf(StatsManager.getStatsCache().getKills(p.getUniqueId()));
                break;
            case "stats_wins":
                replay = String.valueOf(StatsManager.getStatsCache().getWins(p.getUniqueId()));
                break;
            case "stats_finalkills":
                replay = String.valueOf(StatsManager.getStatsCache().getFinalKills(p.getUniqueId()));
                break;
            case "stats_deaths":
                replay = String.valueOf(StatsManager.getStatsCache().getDeaths(p.getUniqueId()));
                break;
            case "stats_losses":
                replay = String.valueOf(StatsManager.getStatsCache().getLosses(p.getUniqueId()));
                break;
            case "stats_finaldeaths":
                replay = String.valueOf(StatsManager.getStatsCache().getFinalDeaths(p.getUniqueId()));
                break;
            case "stats_bedsdestroyed":
                replay = String.valueOf(StatsManager.getStatsCache().getBedsDestroyed(p.getUniqueId()));
                break;
            case "stats_gamesplayed":
                replay = String.valueOf(StatsManager.getStatsCache().getGamesPlayed(p.getUniqueId()));
                break;
            case "current_online":
                replay = String.valueOf(ArenaData.getTotalPlaying());
                break;
            case "current_arenas":
                replay = String.valueOf(ArenaData.getArenaByIdentifier().size());
                break;
            case "player_level":
                replay = SpigotMain.getLevelSupport().getLevel(p);
                break;
            case "player_progress":
                replay = SpigotMain.getLevelSupport().getProgressBar(p);
                break;
            case "player_xp_formatted":
                replay = SpigotMain.getLevelSupport().getCurrentXpFormatted(p);
                break;
            case "player_xp":
                replay = String.valueOf(SpigotMain.getLevelSupport().getCurrentXp(p));
                break;
            case "player_rerq_xp_formatted":
                replay = SpigotMain.getLevelSupport().getRequiredXpFormatted(p);
                break;
            case "player_rerq_xp":
                replay = String.valueOf(SpigotMain.getLevelSupport().getRequiredXp(p));
                break;
        }
        return replay;
    }
}

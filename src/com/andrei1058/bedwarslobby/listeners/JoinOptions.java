package com.andrei1058.bedwarslobby.listeners;

import com.andrei1058.bedwarslobby.SpigotMain;
import com.andrei1058.bedwarslobby.config.ConfigPath;
import com.andrei1058.bedwarslobby.language.Language;
import com.andrei1058.bedwarslobby.misc.Updater;
import com.andrei1058.bedwarslobby.misc.Utils;
import com.andrei1058.bedwarslobby.scoreboard.SBoard;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.UUID;

import static com.andrei1058.bedwarslobby.SpigotMain.getParty;

public class JoinOptions implements Listener {

    private static HashMap<UUID, String> preLoadedLanguage = new HashMap<>();

    @EventHandler
    public void onLoginEvent(PlayerLoginEvent e) {
        Player p = e.getPlayer();
        final UUID u = p.getUniqueId();
        Bukkit.getScheduler().runTaskAsynchronously(SpigotMain.getInstance(), () -> {
            String iso = SpigotMain.remoteDatabase.getLanguage(u);
            if (Language.isLanguageExist(iso)) {
                if (SpigotMain.config.getYml().getStringList(ConfigPath.GENERAL_CONFIGURATION_DISABLED_LANGUAGES).contains(iso))
                    iso = Language.defaultLanguage.getIso();
                preLoadedLanguage.put(u, iso);
            }
        });
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        if (preLoadedLanguage.containsKey(e.getPlayer().getUniqueId())) {
            Language.setPlayerLanguage(e.getPlayer(), preLoadedLanguage.get(e.getPlayer().getUniqueId()), true);
            preLoadedLanguage.remove(e.getPlayer().getUniqueId());
        }

        if (e.getPlayer().isOp()) {
            if (Updater.isUpdateAvailable()) {
                e.getPlayer().sendMessage("§8[§f" + SpigotMain.getInstance().getName() + "§8]§7§m---------------------------");
                e.getPlayer().sendMessage("");
                TextComponent tc = new TextComponent("§eUpdate available: §6" + Updater.getNewVersion() + " §7§o(click)");
                tc.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://www.spigotmc.org/resources/66642/"));
                e.getPlayer().spigot().sendMessage(tc);
                e.getPlayer().sendMessage("");
                e.getPlayer().sendMessage("§8[§f" + SpigotMain.getInstance().getName() + "§8]§7§m---------------------------");
            }
        }

        if (SpigotMain.config.getBoolean("clear-armor-on-join")) e.getPlayer().getInventory().setArmorContents(null);
        if (SpigotMain.config.getBoolean("disable-join-message")) e.setJoinMessage(null);

        if (SpigotMain.config.getBoolean("spawn-on-join")) {
            if (!(SpigotMain.config.getString("spawn-location").isEmpty() && SpigotMain.config.getString("spawn-location").equals("null"))) {
                try {
                    e.getPlayer().teleport(SpigotMain.config.getConfigLoc("spawn-location"));
                } catch (Exception ex) {
                    SpigotMain.getInstance().getLogger().severe("Could not teleport " + e.getPlayer().getName() + " to spawn location!");
                }
            }
        }

        Utils.sendJoinItems(e.getPlayer());
        if (SpigotMain.config.getBoolean(ConfigPath.GENERAL_CONFIGURATION_LOBBY_SCOREBOARD)) {
            Bukkit.getScheduler().runTaskLater(SpigotMain.getInstance(), () -> new SBoard(e.getPlayer()), 20L);
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        if (SpigotMain.config.getBoolean("disable-join-message")) e.setQuitMessage(null);
        //Manage internal parties.
        if (getParty().isInternal()) {
            if (getParty().hasParty(e.getPlayer())) {
                getParty().removeFromParty(e.getPlayer());
            }
        }
    }
}

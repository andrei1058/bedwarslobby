package com.andrei1058.bedwarslobby.listeners;

import com.andrei1058.bedwarslobby.SpigotMain;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

import static org.bukkit.event.inventory.InventoryAction.HOTBAR_SWAP;
import static org.bukkit.event.inventory.InventoryAction.MOVE_TO_OTHER_INVENTORY;

public class CommandItems implements Listener {

    private static HashMap<String, Long> interactCooldown = new HashMap<>();

    @EventHandler
    //Run commands applied on join-items
    public void onCommandItemUse(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.RIGHT_CLICK_AIR) {

            ItemStack i = SpigotMain.getVersionSupport().getItemInHand(p);

            if (i == null) return;
            if (i.getType() == Material.AIR) return;
            if (!canInteract(p, p.getInventory().getHeldItemSlot())) return;
            if (!SpigotMain.getVersionSupport().isCustomItem(i)) return;

            String[] customData = SpigotMain.getVersionSupport().getCustomData(i).split("_");
            if (customData.length >= 2) {
                if (customData[0].equals("RUNCOMMAND")) {
                    e.setCancelled(true);
                    Bukkit.dispatchCommand(p, customData[1]);
                }
            }

        }
    }

    @EventHandler
    //protect join-items from being dropped if protect-world is false
    public void onItemDrop(PlayerDropItemEvent e) {
        if (e.isCancelled()) return;

        ItemStack i = e.getItemDrop().getItemStack();

        if (i == null) return;
        if (i.getType() == Material.AIR) return;
        if (SpigotMain.config.getBoolean("protect-world")) return;

        if (isCommandItem(i)) e.setCancelled(true);
    }

    @EventHandler
    //Manage command-items when clicked in inventory
    public void onCommandItemClick(InventoryClickEvent e) {
        //block moving from hotBar
        if (e.getAction() == HOTBAR_SWAP && e.getClick() == ClickType.NUMBER_KEY) {
            if (e.getHotbarButton() > -1) {
                ItemStack i = e.getWhoClicked().getInventory().getItem(e.getHotbarButton());
                if (i != null) {
                    if (isCommandItem(i)) {
                        e.setCancelled(true);
                        return;
                    }
                }
            }
        }

        //block moving cursor item outside
        if (e.getCursor() != null) {
            if (e.getCursor().getType() != Material.AIR) {
                if (e.getClickedInventory() == null) {
                    if (isCommandItem(e.getCursor())) {
                        e.getWhoClicked().closeInventory();
                        e.setCancelled(true);
                    }
                } else if (e.getClickedInventory().getType() != e.getWhoClicked().getInventory().getType()) {
                    if (isCommandItem(e.getCursor())) {
                        e.getWhoClicked().closeInventory();
                        e.setCancelled(true);
                    }
                } else {
                    if (isCommandItem(e.getCursor())) e.setCancelled(true);
                }
            }
        }

        //block moving current item outside
        if (e.getCurrentItem() != null) {
            if (e.getCurrentItem().getType() != Material.AIR) {
                if (e.getClickedInventory() == null) {
                    if (isCommandItem(e.getCurrentItem())) {
                        e.getWhoClicked().closeInventory();
                        e.setCancelled(true);
                    }
                } else if (e.getClickedInventory().getType() != e.getWhoClicked().getInventory().getType()) {
                    if (isCommandItem(e.getCurrentItem())) {
                        e.getWhoClicked().closeInventory();
                        e.setCancelled(true);
                    }
                } else {
                    if (isCommandItem(e.getCurrentItem())) e.setCancelled(true);
                }
            }
        }

        //block moving with shift
        if (e.getAction() == MOVE_TO_OTHER_INVENTORY) {
            if (isCommandItem(e.getCurrentItem())) e.setCancelled(true);
        }
    }

    /**
     * Check if an item is command-item.
     */
    private static boolean isCommandItem(ItemStack i) {
        if (i == null) return false;
        if (i.getType() == Material.AIR) return false;
        if (SpigotMain.getVersionSupport().isCustomItem(i)) {
            String[] customData = SpigotMain.getVersionSupport().getCustomData(i).split("_");
            if (customData.length >= 2) {
                if (customData[0].equals("RUNCOMMAND")) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Prevent interact spam.
     */
    private static boolean canInteract(Player player, int slot) {
        if (!interactCooldown.containsKey(player.getName() + "_" + slot)) {
            interactCooldown.put(player.getName() + "_" + slot, System.currentTimeMillis() + 2000);
            return true;
        } else {
            if (interactCooldown.get(player.getName() + "_" + slot) <= System.currentTimeMillis()) {
                interactCooldown.replace(player.getName() + "_" + slot, System.currentTimeMillis() + 2000);
                return true;
            }
            return false;
        }
    }
}

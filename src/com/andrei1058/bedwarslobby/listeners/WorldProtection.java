package com.andrei1058.bedwarslobby.listeners;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class WorldProtection implements Listener {

    //players that can build /bw build
    private static List<UUID> buildSession = new ArrayList<>();

    @EventHandler
    public void onIceMelt(BlockFadeEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onCactus(BlockPhysicsEvent e) {
        if (e.getBlock().getType() == Material.CACTUS) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        if (!buildSession.contains(e.getPlayer().getUniqueId())) {
            e.setBuild(false);
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e){
        if (!buildSession.contains(e.getPlayer().getUniqueId())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBucketFill(PlayerBucketFillEvent e){
        if (!buildSession.contains(e.getPlayer().getUniqueId())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBucketEmpty(PlayerBucketEmptyEvent e){
        if (!buildSession.contains(e.getPlayer().getUniqueId())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onEntityExplode(EntityExplodeEvent e){
        e.blockList().clear();
        e.setCancelled(true);
    }

    @EventHandler
    public void onBlockExplode(BlockExplodeEvent e){
        e.setCancelled(true);
    }

    @EventHandler
    public void onPaintingRemove(HangingBreakByEntityEvent e){
        if (e.getRemover().getType() == EntityType.PLAYER){
            Player p = (Player) e.getRemover();
            if (!buildSession.contains(p.getUniqueId())) {
                e.setCancelled(true);
            }
            return;
        }
        e.setCancelled(true);
    }

    @EventHandler
    public void soildChange(EntityChangeBlockEvent e){
        if (e.getTo() == Material.DIRT){
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onWeatherChange(WeatherChangeEvent e){
        if (e.toWeatherState()) e.setCancelled(true);
    }

    @EventHandler
    public void onCreatureSpawn(CreatureSpawnEvent e){
        if (!(e.getSpawnReason() == CreatureSpawnEvent.SpawnReason.CUSTOM ||
                e.getSpawnReason() == CreatureSpawnEvent.SpawnReason.EGG)) e.setCancelled(true);
    }

    @EventHandler
    public void onItemDrop(PlayerDropItemEvent e) {
        e.setCancelled(true);
    }

    public static List<UUID> getBuildSession() {
        return buildSession;
    }
}

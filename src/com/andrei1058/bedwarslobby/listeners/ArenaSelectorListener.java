package com.andrei1058.bedwarslobby.listeners;

import com.andrei1058.bedwarslobby.SpigotMain;
import com.andrei1058.bedwarslobby.misc.Utils;
import com.andrei1058.bedwarslobby.arena.ArenaData;
import com.andrei1058.bedwarslobby.arena.ArenaGUI;
import com.andrei1058.bedwarslobby.arena.ArenaStatus;
import com.andrei1058.bedwarslobby.language.Language;
import com.andrei1058.bedwarslobby.language.Messages;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;

public class ArenaSelectorListener implements Listener {

    public static final String ARENA_SELECTOR_GUI_IDENTIFIER = "arena=";

    @EventHandler
    public void onArenaSelectorClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        if (!ArenaGUI.getRefresh().containsKey(p)) return;
        e.setCancelled(true);
        ItemStack i = e.getCurrentItem();

        if (i == null) return;
        if (i.getType() == Material.AIR) return;

        if (!SpigotMain.getVersionSupport().isCustomItem(i)) return;
        String data = SpigotMain.getVersionSupport().getCustomData(i);
        if (!data.contains(ARENA_SELECTOR_GUI_IDENTIFIER)) return;
        String arena = data.split("=")[1];
        ArenaData a = ArenaData.getArenaByBungeeName(arena);
        if (a == null) return;
        if (a.getStatus() == ArenaStatus.OFFLINE) return;

        if (e.getClick() == ClickType.LEFT) {
            if (a.getStatus() == ArenaStatus.WAITING || a.getStatus() == ArenaStatus.STARTING) {
                Utils.sendPlayerToServer(p, a, false);
            } else {
                p.sendMessage(SpigotMain.config.m("messages.join-denied-selector"));
            }
        } else if (e.getClick() == ClickType.RIGHT) {
            if (a.getStatus() == ArenaStatus.PLAYING) {
                if (SpigotMain.getParty().hasParty(p)){
                    p.sendMessage(Language.getMsg(p, Messages.ARENA_SPECTATE_DENIED_SELECTOR));
                    return;
                }
                Utils.sendPlayerToServer(p, a, false);
            } else {
                p.sendMessage(Language.getMsg(p, Messages.ARENA_SPECTATE_DENIED_SELECTOR));
            }
        }

        p.closeInventory();
    }

    @EventHandler
    public void onArenaSelectorClose(InventoryCloseEvent e) {
        ArenaGUI.getRefresh().remove(e.getPlayer());
    }
}

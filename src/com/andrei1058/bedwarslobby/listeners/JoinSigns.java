package com.andrei1058.bedwarslobby.listeners;

import com.andrei1058.bedwarslobby.SpigotMain;
import com.andrei1058.bedwarslobby.misc.Utils;
import com.andrei1058.bedwarslobby.arena.ArenaData;
import com.andrei1058.bedwarslobby.arena.ArenaStatus;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class JoinSigns implements Listener {

    private static HashMap<UUID, Long> interactCooldown = new HashMap<>();

    /**
     * update game signs
     */
    @EventHandler
    public void onSignChange(SignChangeEvent e) {
        if (!WorldProtection.getBuildSession().contains(e.getPlayer().getUniqueId())) {
            e.setCancelled(true);
            return;
        }
        Player p = e.getPlayer();
        if (e.getLine(0).equalsIgnoreCase("[bw]")) {

            if (!e.getPlayer().hasPermission("bw.staff")) {
                p.sendMessage("§c▪ §7You can't do that!");
                e.setCancelled(true);
                return;
            }

            List<String> joinSigns = SpigotMain.config.getYml().getStringList("join-signs.locations");
            joinSigns.add(e.getLine(1) + "," + SpigotMain.config.getConfigLoc(e.getBlock().getLocation()));
            SpigotMain.config.set("join-signs.locations", joinSigns);

            ArenaData ad = ArenaData.getArenaByBungeeName(e.getLine(1));

            Sign b = (Sign) e.getBlock().getState();

            if (ad == null){
                Bukkit.getScheduler().runTaskLater(SpigotMain.getInstance(), ()-> {
                    Utils.setOfflineSign(b, e.getLine(1));
                }, 20L);
                return;
            }
            ad.addSign(e.getBlock().getLocation());

            int line = 0;
            for (String string : SpigotMain.config.l("join-signs.format." + ad.getStatus().toString().toString())) {
                e.setLine(line, string.replace("[on]", String.valueOf(ad.getCurrentPlayers())).replace("[max]",
                        String.valueOf(ad.getMaxPlayers())).replace("[arena]", ad.getArenaName()).replace("[status]",
                        ad.getDisplayStatus(p)));
                line++;
            }
            b.update(true);
        }
    }

    @EventHandler
    public void onSignInteract(PlayerInteractEvent e) {
        Block b = e.getClickedBlock();
        if (b == null) return;
        if (b.getType() == Material.AIR) return;
        if (b.getState() instanceof Sign) {
            if (!canInteract(e.getPlayer())) return;
            for (ArenaData a1 : ArenaData.getArenaByIdentifier().values()) {
                if (a1.getSigns().contains(b.getState())) {
                    if (a1.getStatus() != ArenaStatus.RESTARTING)
                        Utils.sendPlayerToServer(e.getPlayer(), a1, false);
                    return;
                }
            }
        }
    }

    /**
     * Update join signs.
     */
    public static void updateJoinSigns() {
        for (ArenaData ad : ArenaData.getArenaByIdentifier().values()) {
            ad.refreshSigns();
        }
    }

    /**
     * Prevent interact spam.
     */
    private static boolean canInteract(Player player) {
        if (!interactCooldown.containsKey(player.getUniqueId())) {
            interactCooldown.put(player.getUniqueId(), System.currentTimeMillis() + 2000);
            return true;
        } else {
            if (interactCooldown.get(player.getUniqueId()) <= System.currentTimeMillis()) {
                interactCooldown.replace(player.getUniqueId(), System.currentTimeMillis() + 2000);
                return true;
            }
            return false;
        }
    }
}

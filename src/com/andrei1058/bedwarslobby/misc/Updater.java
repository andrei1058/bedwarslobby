package com.andrei1058.bedwarslobby.misc;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Updater {

    private static boolean updateAvailable = false;
    private static String newVersion = "";


    public static void checkUpdate(Plugin plugin) {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try {
                HttpURLConnection checkUpdate = (HttpURLConnection) new URL("https://api.spigotmc.org/legacy/update.php?resource=66642").openConnection();
                checkUpdate.setDoOutput(true);
                String old = plugin.getDescription().getVersion();
                String newV = new BufferedReader(new InputStreamReader(checkUpdate.getInputStream())).readLine();
                if (!newV.equalsIgnoreCase(old)) {
                    updateAvailable = true;
                    newVersion = newV;
                    plugin.getLogger().info("                                    ");
                    plugin.getLogger().info("                                    ");
                    plugin.getLogger().info("                                    ");
                    plugin.getLogger().info("                                    ");
                    plugin.getLogger().info("                                    ");
                    plugin.getLogger().info("------------------------------------");
                    plugin.getLogger().info(" ");
                    plugin.getLogger().info("There is a new version available!");
                    plugin.getLogger().info("Version: " + newVersion);
                    plugin.getLogger().info(" ");
                    plugin.getLogger().info("https://www.spigotmc.org/resources/66642/");
                    plugin.getLogger().info("------------------------------------");
                    plugin.getLogger().info("                                    ");
                    plugin.getLogger().info("                                    ");
                    plugin.getLogger().info("                                    ");
                    plugin.getLogger().info("                                    ");
                    plugin.getLogger().info("                                    ");
                }
            } catch (IOException e) {
                plugin.getLogger().severe("Couldn't check for updates!");
            }
        });
    }

    public static boolean isUpdateAvailable() {
        return updateAvailable;
    }

    public static String getNewVersion() {
        return newVersion;
    }
}

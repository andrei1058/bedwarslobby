package com.andrei1058.bedwarslobby.scoreboard;

public class Refresh implements Runnable {

    @Override
    public void run() {
        for (SBoard sb : SBoard.getScoreboards()) {
            sb.refresh();
        }
    }
}

package com.andrei1058.bedwarslobby.scoreboard;

import com.andrei1058.bedwarslobby.SpigotMain;
import com.andrei1058.bedwarslobby.arena.ArenaData;
import com.andrei1058.bedwarslobby.language.Language;
import com.andrei1058.bedwarslobby.language.Messages;
import com.andrei1058.bedwarslobby.papi.SupportPAPI;
import com.andrei1058.bedwarslobby.stats.StatsManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

import java.text.SimpleDateFormat;
import java.util.*;

import static com.andrei1058.bedwarslobby.language.Language.getMsg;

public class SBoard {

    public static ScoreboardManager sbm = Bukkit.getScoreboardManager();
    private List<String> placeholders = Arrays.asList("{on}", "{date}");
    private static List<SBoard> scoreboards = new ArrayList<>();
    private HashMap<Team, String> toRefresh = new HashMap<>();
    private Scoreboard sb = sbm.getNewScoreboard();
    private Player p;
    private Objective o;
    private SimpleDateFormat dateFormat;

    /**
     * Used for spectators
     */
    public SBoard(Player p) {
        this.p = p;
        o = sb.registerNewObjective("Sb", "or");
        o.setDisplaySlot(DisplaySlot.SIDEBAR);
        dateFormat = new SimpleDateFormat(getMsg(p, Messages.FORMATTING_SCOREBOARD_DATE));
        this.setStrings(Language.getList(p, Messages.SCOREBOARD_LOBBY));
        Bukkit.getScheduler().runTaskLater(SpigotMain.getInstance(), () -> {
            p.setScoreboard(sb);
            scoreboards.add(this);
        }, 10L);
    }

    public void setStrings(List<String> strings) {
        for (Team t1 : sb.getTeams()) {
            t1.unregister();
        }
        toRefresh.clear();
        o.setDisplayName(strings.get(0));
        int score = 0;
        for (int x = strings.size(); x > 1; x--) {
            Team t = sb.registerNewTeam("team" + x);
            t.addEntry(ChatColor.values()[x - 1].toString());
            o.getScore(ChatColor.values()[x - 1].toString()).setScore(score);
            score++;
            String temp = strings.get(x - 1);
            temp = temp.replace("{generatorUpgrade}", "{nextEvent}")
                    .replace("{generatorTimer}", "{time}");
            temp = temp.replace("{level}", SpigotMain.getLevelSupport().getLevel(p));
            temp = temp.replace("{progress}", SpigotMain.getLevelSupport().getProgressBar(p));
            temp = temp.replace("{currentXp}", SpigotMain.getLevelSupport().getCurrentXpFormatted(p));
            temp = temp.replace("{requiredXp}", SpigotMain.getLevelSupport().getRequiredXpFormatted(p));
            for (String ph : placeholders) {
                if (temp.contains(ph)) {
                    if (!toRefresh.containsKey(t)) {
                        toRefresh.put(t, temp);
                    }
                }
            }
            temp = temp.replace("{server}", Bukkit.getServer().getMotd()).replace("{on}", String.valueOf(Bukkit.getOnlinePlayers().size()+ ArenaData.getTotalPlaying()))
                    .replace("{max}", String.valueOf(Bukkit.getServer().getMaxPlayers())).replace("{date}",
                            new SimpleDateFormat(getMsg(getP(), Messages.FORMATTING_SCOREBOARD_DATE)).format(new Date(System.currentTimeMillis())))
                    .replace("{money}", String.valueOf(SpigotMain.getEconomy().getMoney(p)));

            setContent(t, StatsManager.replaceStatsPlaceholders(getP(), temp, true));
        }
    }

    private void setContent(Team t, String s) {
        s = SupportPAPI.getSupportPAPI().replace(getP(), s);
        if (s.length() >= 16) {
            String prefix = s.substring(0, 16);
            String suffix;
            if (prefix.endsWith("&") || prefix.endsWith("§")) {
                prefix = prefix.substring(0, prefix.length() - 1);
                suffix = s.substring(prefix.length());
            } else if (prefix.substring(0, 15).endsWith("&") || prefix.substring(0, 15).endsWith("§")) {
                prefix = prefix.substring(0, prefix.length() - 2);
                suffix = s.substring(prefix.length());
            } else {
                suffix = ChatColor.getLastColors(prefix) + s.substring(prefix.length());
            }
            if (suffix.length() > 16) {
                suffix = suffix.substring(0, 16);
            }
            t.setPrefix(prefix);
            t.setSuffix(suffix);
        } else {
            t.setPrefix(s);
            t.setSuffix("");
        }
    }


    public void refresh() {
        String date = new SimpleDateFormat(getMsg(getP(), Messages.FORMATTING_SCOREBOARD_DATE)).format(new Date(System.currentTimeMillis()));
        for (Map.Entry<Team, String> e : toRefresh.entrySet()) {
            setContent(e.getKey(), e.getValue().replace("{on}", String.valueOf(Bukkit.getOnlinePlayers().size()))
                    .replace("{max}", String.valueOf(Bukkit.getServer().getMaxPlayers())).replace("{date}", date));
        }
    }

    public Player getP() {
        return p;
    }

    public void remove() {
        p.setScoreboard(sbm.getNewScoreboard());
        scoreboards.remove(this);
    }

    public static List<SBoard> getScoreboards() {
        return scoreboards;
    }
}
